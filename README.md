<div align="center">

### boardgamegeek

[![build status](https://gitlab.com/haath/boardgamegeek-haxe/badges/master/pipeline.svg)](https://gitlab.com/haath/boardgamegeek-haxe/pipelines)
[![test coverage](https://gitlab.com/haath/boardgamegeek-haxe/badges/master/coverage.svg)](https://gitlab.com/haath/boardgamegeek-haxe/-/jobs/artifacts/master/browse?job=test-neko)
[![license](https://img.shields.io/badge/license-MIT-blue.svg?style=flat)](https://gitlab.com/haath/boardgamegeek-haxe/blob/master/LICENSEs)
[![haxelib version](https://badgen.net/haxelib/v/boardgamegeek)](https://lib.haxe.org/p/boardgamegeek)
[![haxelib downloads](https://badgen.net/haxelib/d/boardgamegeek)](https://lib.haxe.org/p/boardgamegeek)

</div>

---

This is a simple Haxe implementation for fetching data from the [BoardGameGeek](https://boardgamegeek.com) API.


## Installation

The library is available on [Haxelib](https://lib.haxe.org).

```sh
haxelib install boardgamegeek
```

Add `tink_xml` as a git library. Haxelib does not currently allow git dependencies in `haxelib.json`.

```haxe
--library tink_xml:git:https://github.com/haxetink/tink_xml.git
```


## Usage

All the API interactions are implemented in the `BoardGameGeek` class.

Note that all methods return [tink_core](https://github.com/haxetink/tink_core) promises.
As such they are compatible with the rest of the tink framework.
For the examples here, it is assumed that [tink_await](https://github.com/haxetink/tink_await) is also installed, but it is not a dependency of this library.


```haxe
var bgg: BoardGameGeek = new BoardGameGeek();

// get a specific board game by id
var game: Thing = @await bgg.getThing(283355);
game.name;
game.categories;

// get all of the game's expansions
var expansions: Array<Thing> = @await bgg.getExpansions(game);

// search for a board game by name
var results: Array<SearchResult> = @await bgg.search('Carcassone', BoardGameExpansion);

// get a user's public profile
var user: User = @await bgg.getUser('Haath');
```


#### Collections

User collections are fetched using a queue system.
This means that on the first request, BGG will put us in a queue while it fetches the data.

```haxe
// get a user's collection
var collection: UserCollection = null;
while (true)
{
    collection = @await bgg.getCollection('Haath');
    if (collection.items != null)
    {
        // got the result!
        break;
    }

    // our request is in the queue, try again after 1sec
    @await Future.delay(1000, 0);

}
```


#### Rate limiting

What is returned by the promise will be:

- `Success(data)` if the request was successful, and `data` is the returned object.
- `Success(null)` if the rate limiting error codes were returned by the API.
- `Failure(err)` on any other error `err`.
