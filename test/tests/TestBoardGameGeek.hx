package tests;

import boardgamegeek.ApiBaseUrl;
import boardgamegeek.BoardGameGeek;
import boardgamegeek.types.Family;
import boardgamegeek.types.HotItem;
import boardgamegeek.types.SearchResult;
import boardgamegeek.types.Thing;
import boardgamegeek.types.ThingType;
import boardgamegeek.types.User;
import boardgamegeek.types.UserCollection;
import tink.CoreApi;
import utest.Assert;
import utest.Async;
import utest.ITest;

using TestUtil;


@await
class TestBoardGameGeek implements ITest
{
    var bgg: BoardGameGeek;

    public function new()
    {
    }

    function setup()
    {
        // alternate randomly between the available urls in case this helps with rate limiting
        var rand: Float = Math.random();
        var baseUrl: ApiBaseUrl = if (rand < 0.33)
            {
                BoardGameGeek;
            }
            else if (rand < 0.66)
            {
                RpgGeek;
            }
            else
            {
                VideoGameGeek;
            }

        bgg = new BoardGameGeek(baseUrl);
    }

    @:timeout(5000) @await
    function testGetThing(async: Async)
    {
        var thing: Thing = @await bgg.getThing(283355);

        Assert.equals(283355, thing.id);
        Assert.equals(2019, thing.year);
        Assert.equals('Dune', thing.name);
        Assert.equals(ThingType.BoardGame, thing.type);
        Assert.equals(2, thing.minPlayers);
        Assert.equals(6, thing.maxPlayers);
        Assert.equals(180, thing.playTime);
        Assert.equals(120, thing.minPlayTime);
        Assert.equals(180, thing.maxPlayTime);

        Assert.isTrue(thing.categories.contains('Bluffing'));
        Assert.isTrue(thing.categories.contains('Fighting'));
        Assert.isTrue(thing.categories.contains('Negotiation'));

        Assert.isTrue(thing.mechanics.contains('Bribery'));
        Assert.isTrue(thing.mechanics.contains('Area Movement'));

        Assert.isTrue(thing.families.contains('Books: Dune'));
        Assert.isTrue(thing.families.contains('Theme: Deserts'));

        Assert.isTrue(thing.expansions.exists(c -> c.name == 'Dune: Ixians & Tleilaxu'));
        Assert.isTrue(thing.expansions.exists(c -> c.name == 'Dune: Ecaz & Moritani'));

        Assert.isTrue(thing.designers.contains('Bill Eberle'));
        Assert.isTrue(thing.designers.contains('Jack Kittredge'));

        Assert.isTrue(thing.artists.contains('Ilya Baranovsky'));
        Assert.isTrue(thing.publishers.contains('Gale Force Nine, LLC'));

        async.done();
    }

    @:timeout(5000) @await
    function testGetExpansions(async: Async)
    {
        var thing: Thing = @await bgg.getThing(237182);

        Assert.equals(237182, thing.id);
        Assert.equals(2018, thing.year);
        Assert.equals('Root', thing.name);

        var expansions: Array<Thing> = @await bgg.getExpansions(thing);
        Assert.isTrue(expansions.exists(e -> e.id == 374676));
        Assert.isTrue(expansions.exists(e -> e.id == 336276));
        Assert.isTrue(expansions.exists(e -> e.id == 241386));
        Assert.isTrue(expansions.exists(e -> e.id == 309977));
        Assert.isTrue(expansions.exists(e -> e.id == 361193));

        async.done();
    }

    @:timeout(5000) @await
    function testGetExpansionsNonBoardGame(async: Async)
    {
        var thing: Thing = @await bgg.getThing(361193);

        Assert.raises(() -> bgg.getExpansions(thing));

        async.done();
    }

    @:timeout(5000) @await
    function testGetThings(async: Async)
    {
        var things: Array<Thing> = @await bgg.getThings([ 350184, 332772 ]);

        Assert.equals(350184, things[ 0 ].id);
        Assert.equals(332772, things[ 1 ].id);

        async.done();
    }

    @:timeout(5000) @await
    function testGetFamily(async: Async)
    {
        var family: Family = @await bgg.getFamily(5838);

        Assert.equals(5838, family.id);
        Assert.equals('Books: Dune', family.name);
        Assert.isTrue(family.relatedFamilies.contains('CHOAM'));
        Assert.isTrue(family.relatedFamilies.contains('Conqueror of Dune'));

        async.done();
    }

    @:timeout(5000) @await
    function testGetUser(async: Async)
    {
        var user: User = @await bgg.getUser('Haath');

        Assert.equals('Greg', user.firstName);
        Assert.equals('Mnt', user.lastName);
        Assert.equals('Prague', user.stateOrProvince);
        Assert.equals('Czech Republic', user.country);

        async.done();
    }

    @:timeout(10000) @await
    function testGetUserCollection(async: Async)
    {
        var attempts: Int = 10;

        while (--attempts > 0)
        {
            var collection: UserCollection = @await bgg.getUserCollection('Haath');
            if (collection.items == null)
            {
                @await Future.delay(1000, true);
                continue;
            }

            Assert.isTrue(collection.items.length > 0);
            Assert.isTrue(collection.items.exists(i -> i.name == 'Arkham Horror: The Card Game'));
            Assert.isTrue(collection.items.exists(i -> i.name == 'Concordia'));
            Assert.isTrue(collection.items.exists(i -> i.name == 'Dune' && i.owned));
            Assert.isTrue(collection.items.exists(i -> i.name == 'Dune' && i.owned && i.image != "" && i.image != null));
            Assert.isTrue(collection.items.exists(i -> i.name == 'Eclipse' && i.wishlist));
            break;
        }

        Assert.isTrue(attempts > 0);

        async.done();
    }

    @:timeout(10000) @await
    function testSearch(async: Async)
    {
        var results: Array<SearchResult> = @await bgg.search('arkham horror');

        Assert.isTrue(results.length > 0);

        Assert.isTrue(results.exists(r -> r.type == BoardGame
            && r.id == 257499
            && r.name == 'Arkham Horror (Third Edition)'
            && r.year == 2018));

        async.done();
    }

    @:timeout(1000) @await
    function testGetHot(async: Async)
    {
        var hotGames: Array<HotItem> = @await bgg.getHotItems(BoardGame);

        for (rank in 1...51)
        {
            Assert.isTrue(hotGames.exists(g -> g.rank == rank));
        }

        for (game in hotGames)
        {
            Assert.isTrue(game.year > 0);
            Assert.notNull(game.name);
            Assert.notNull(game.thumbnail);
        }

        async.done();
    }
}
