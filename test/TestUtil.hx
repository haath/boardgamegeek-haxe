class TestUtil
{
    public static function contains<T>(arr: Array<T>, item: T): Bool
    {
        return arr.indexOf(item) > -1;
    }

    public static function exists<T>(arr: Array<T>, cb: T->Bool): Bool
    {
        for (i in arr)
        {
            if (cb(i))
            {
                return true;
            }
        }
        return false;
    }
}
