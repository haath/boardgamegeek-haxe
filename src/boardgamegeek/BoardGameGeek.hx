package boardgamegeek;

import haxe.Http;
import tink.CoreApi;
import tink.xml.Source;
import tink.xml.Structure;
import boardgamegeek.types.*;
import boardgamegeek.types.User.UserData;


class BoardGameGeek
{
    final baseUrl: ApiBaseUrl;

    public function new(baseUrl: ApiBaseUrl = BoardGameGeek)
    {
        this.baseUrl = baseUrl;
    }

    /**
     * Fetch an item from the BoardGameGeek database by its id.
     *
     * @param id the item's id
     * @return the item
     */
    public function getThing(id: UInt): Promise<Thing>
    {
        return
            getThings([ id ]).map(o -> switch o
            {
                case Success(null):
                    Success(null);

                case Success(things):
                    Success(things[ 0 ]);

                case Failure(failure):
                    Failure(failure);
            });
    }

    /**
     * Fetch multiple items from the BoardGameGeek database by their ids.
     *
     * @param ids the item ids
     * @return the items
     */
    public function getThings(ids: Array<UInt>): Promise<Array<Thing>>
    {
        return
            httpGet(Thing, [ 'id' => ids.join(',') ]).map(o -> switch o
            {
                case Success(null):
                    Success(null);

                case Success(data):
                    switch new Structure<ThingList>().read(Source.fromString(data))
                    {
                        case Success(obj):
                            Success(obj.things);

                        case Failure(failure):
                            Failure(cast failure);
                    }

                case Failure(failure):
                    Failure(failure);
            });
    }

    /**
     * Fetch the expansions of a board game.
     *
     * @param boardGame the board game, as it is returned by the `getItem()` method
     * @return the board game's expansions
     */
    public function getExpansions(boardGame: Thing): Promise<Array<Thing>>
    {
        if (boardGame.type != BoardGame)
        {
            throw 'cant get the expansions for a thing that is not a board gane';
        }

        var expansionIds: Array<UInt> = boardGame.expansions.map(e -> e.id);

        return getThings(expansionIds);
    }

    /**
     * Fetch a family from the BoardGameGeek database by its id.
     *
     * @param ids the family id
     * @return the family
     */
    public function getFamily(id: UInt): Promise<Family>
    {
        return
            httpGet(Family, [ 'id' => Std.string(id) ]).map(o -> switch o
            {
                case Success(null):
                    Success(null);

                case Success(data):
                    switch new Structure<FamilyList>().read(Source.fromString(data))
                    {
                        case Success(obj):
                            Success(obj.things[ 0 ]);

                        case Failure(failure):
                            Failure(cast failure);
                    }

                case Failure(failure):
                    Failure(failure);
            });
    }

    /**
     * Fetch the public profile information of a user by their username.
     *
     * @param name the user's username
     * @return the user public profile information
     */
    public function getUser(name: String): Promise<User>
    {
        return
            httpGet(User, [ 'name' => name ]).map(o -> switch o
            {
                case Success(null):
                    Success(null);

                case Success(data):
                    switch new Structure<UserData>().read(Source.fromString(data))
                    {
                        case Success(obj):
                            Success(obj);

                        case Failure(failure):
                            Failure(cast failure);
                    }

                case Failure(failure):
                    Failure(failure);
            });
    }

    /**
     * Fetch the public collection of the user.
     *
     * Note that if the returned collection is `null`, it means that the rate limiting has been reached.
     * If the collection is not `null`, but `collection.items` is `null`, then that means that the request is queued
     * and should be performed again after some delay.
     *
     * @param name the user's username
     * @return the user's collection
     */
    public function getUserCollection(name: String): Promise<UserCollection>
    {
        return
            httpGet(Collection, [ 'username' => name ]).map(o -> switch o
            {
                case Success(null):
                    Success(null);

                case Success('queued'):
                    Success({items: null});

                case Success(data):
                    switch new Structure<CollectionThingList>().read(Source.fromString(data))
                    {
                        case Success(list):
                            Success({items: list.items});

                        case Failure(failure):
                            Failure(cast failure);
                    }

                case Failure(failure):
                    Failure(failure);
            });
    }

    /**
     * Search the BoardGameGeek database for items matching a given query.
     *
     * @param query the query to search for
     * @param type the type of items to limit the search to, by default all types of items are returned
     * @param exact set to `true` to limit the search only to items that match the given `query` exactly
     * @return Promise<Array<SearchResult>>
     */
    public function search(query: String, type: ThingType = null, exact: Bool = false): Promise<Array<SearchResult>>
    {
        return
            httpGet(Search, [ 'query' => query, 'type' => type, 'exact' => exact ? '1' : '0' ]).map(o -> switch o
            {
                case Success(null):
                    Success(null);

                case Success(data):
                    switch new Structure<SearchResultList>().read(Source.fromString(data))
                    {
                        case Success(obj):
                            Success(obj.items);

                        case Failure(failure):
                            Failure(cast failure);
                    }

                case Failure(failure):
                    Failure(failure);
            });
    }

    /**
     * Fetch a list of the currently most active items on BoardGameGeek.
     *
     * @param type the type of items to list
     * @return the list of hot items
     */
    public function getHotItems(type: HotItemType = BoardGame): Promise<Array<HotItem>>
    {
        return
            httpGet(Hot, [ 'type' => type ]).map(o -> switch o
            {
                case Success(null):
                    Success(null);

                case Success(data):
                    switch new Structure<HotItemList>().read(Source.fromString(data))
                    {
                        case Success(obj):
                            Success(obj.items);

                        case Failure(failure):
                            Failure(cast failure);
                    }

                case Failure(failure):
                    Failure(failure);
            });
    }

    function httpGet(method: ApiMethod, parameters: Map<String, String>): Promise<String>
    {
        var req: Http = new Http('$baseUrl/xmlapi2/$method');

        for (name => value in parameters)
        {
            if (value != null)
            {
                req.addParameter(name, value);
            }
        }


        var resp: PromiseTrigger<String> = Promise.trigger();

        req.onStatus = (status: Int) ->
        {
            switch status
            {
                case 200:
                // success

                case 202:
                    // request is queued
                    resp.trigger(Success('queued'));


                case 500 | 503:
                    // rate limiting
                    resp.trigger(Success(null));

                default:
                    resp.trigger(Failure(new Error(status, 'http error')));
            };
        };
        req.onData = (data: String) ->
        {
            resp.trigger(Success(data));
        };
        req.request();

        return resp.asFuture();
    }
}
