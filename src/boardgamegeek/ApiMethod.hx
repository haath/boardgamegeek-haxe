package boardgamegeek;

enum abstract ApiMethod(String)
{
    var Thing = 'thing';
    var Family = 'family';
    var ForumList = 'forumlist';
    var Forum = 'forum';
    var Thread = 'thread';
    var User = 'user';
    var Guild = 'guild';
    var Plays = 'plays';
    var Collection = 'collection';
    var Hot = 'hot';
    var Search = 'search';
}
