package boardgamegeek.types;

import boardgamegeek.types.ValueTagInt;
import boardgamegeek.types.ValueTagString;


typedef HotItemData =
{
    @:attr var id: UInt;
    @:attr var rank: UInt;
    var thumbnail: ValueTagString;
    var name: ValueTagString;
    var yearpublished: ValueTagInt;
}

@:forward(id, rank)
abstract HotItem(HotItemData) from HotItemData
{
    public var name(get, never): String;

    inline function get_name(): String
    {
        return this.name.value;
    }

    public var thumbnail(get, never): String;

    inline function get_thumbnail(): String
    {
        return this.thumbnail.value;
    }

    public var year(get, never): Null<UInt>;

    inline function get_year(): Null<UInt>
    {
        return this.yearpublished?.value;
    }
}
