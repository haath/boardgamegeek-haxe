package boardgamegeek.types;

enum abstract HotItemType(String) to String
{
    var BoardGame = 'boardgame';
    var Rpg = 'rpg';
    var VideoGame = 'videogame';
    var BoardGamePerson = 'boardgameperson';
    var RpgPerson = 'rpgperson';
    var BoardGameCompany = 'boardgamecompany';
    var RpgCompany = 'rpgcompany';
    var VideoGameCompany = 'videogamecompany';
}
