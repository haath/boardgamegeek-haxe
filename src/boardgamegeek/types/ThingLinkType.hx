package boardgamegeek.types;

enum abstract ThingLinkType(String) from String to String
{
    var BoardGameCategory = 'boardgamecategory';
    var BoardGameMechanic = 'boardgamemechanic';
    var BoardGameFamily = 'boardgamefamily';
    var BoardGameExpansion = 'boardgameexpansion';
    var BoardGameAccessory = 'boardgameaccessory';
    var BoardGameImplementation = 'boardgameimplementation';
    var BoardGameDesigner = 'boardgamedesigner';
    var BoardGameArtist = 'boardgameartist';
    var BoardGamePublisher = 'boardgamepublisher';
}
