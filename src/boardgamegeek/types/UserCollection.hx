package boardgamegeek.types;

typedef UserCollection =
{
    /**
     * The list of items in the user's collection.
     *
     * If it is `null`, then the request for the collection has been queued by BGG and should be attempted again.
     *
     * If it is an empty list, then the user has an empty collection.
     */
    var items: Array<CollectionThing>;
}
