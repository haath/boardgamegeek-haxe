package boardgamegeek.types;

typedef FamilyData =
{
    @:attr var id: Int;
    @:attr var type: String;
    var thumbnail: String;
    var image: String;
    var description: String;
    @:list('name') var names: Array<ValueTagString>;

    @:list('link') var links: Array<ThingLink>;
}

@:forward(id, thumbnail, image, description)
abstract Family(FamilyData) from FamilyData
{
    public var name(get, never): String;

    inline function get_name(): String
    {
        return this.names.filter(n -> n.type == 'primary')[ 0 ].value;
    }

    public var type(get, never): ThingType;

    inline function get_type(): ThingType
    {
        return this.type;
    }

    public var relatedFamilies(get, never): Array<String>;

    inline function get_relatedFamilies(): Array<String>
    {
        return this.links.filter(l -> l.type == ThingLinkType.BoardGameFamily).map(l -> l.value);
    }
}
