package boardgamegeek.types;

typedef ValueTagInt =
{
    @:attr var value: Int;
}
