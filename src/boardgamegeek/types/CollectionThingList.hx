package boardgamegeek.types;

import boardgamegeek.types.CollectionThing.CollectionThingData;


typedef CollectionThingList =
{
    @:list('item') var items: Array<CollectionThingData>;
}
