package boardgamegeek.types;

import boardgamegeek.types.ValueTagInt;


typedef UserData =
{
    @:attr var id: UInt;
    @:attr var name: String;

    var firstname: ValueTagString;
    var lastname: ValueTagString;
    var avatarlink: ValueTagString;
    var yearregistered: ValueTagInt;
    var lastlogin: ValueTagString;
    var stateorprovince: ValueTagString;
    var country: ValueTagString;
    var webaddress: ValueTagString;
    var xboxaccount: ValueTagString;
    var wiiaccount: ValueTagString;
    var psnaccount: ValueTagString;
    var battlenetaccount: ValueTagString;
    var steamaccount: ValueTagString;
    var traderating: ValueTagInt;
}

@:forward(id, name)
abstract User(UserData) from UserData
{
    public var firstName(get, never): String;

    inline function get_firstName(): String
    {
        return this.firstname.value;
    }

    public var lastName(get, never): String;

    inline function get_lastName(): String
    {
        return this.lastname.value;
    }

    public var avatarLink(get, never): String;

    inline function get_avatarLink(): String
    {
        return this.avatarlink.value;
    }

    public var yearRegistered(get, never): UInt;

    inline function get_yearRegistered(): UInt
    {
        return this.yearregistered.value;
    }

    public var lastLogin(get, never): String;

    inline function get_lastLogin(): String
    {
        return this.lastlogin.value;
    }

    public var stateOrProvince(get, never): String;

    inline function get_stateOrProvince(): String
    {
        return this.stateorprovince.value;
    }

    public var country(get, never): String;

    inline function get_country(): String
    {
        return this.country.value;
    }

    public var webAddress(get, never): String;

    inline function get_webAddress(): String
    {
        return this.webaddress.value;
    }

    public var xboxAccount(get, never): String;

    inline function get_xboxAccount(): String
    {
        return this.xboxaccount.value;
    }

    public var wiiAccount(get, never): String;

    inline function get_wiiAccount(): String
    {
        return this.wiiaccount.value;
    }

    public var psnAccount(get, never): String;

    inline function get_psnAccount(): String
    {
        return this.psnaccount.value;
    }

    public var steamAccount(get, never): String;

    inline function get_steamAccount(): String
    {
        return this.steamaccount.value;
    }

    public var traderRating(get, never): UInt;

    inline function get_traderRating(): UInt
    {
        return this.traderating.value;
    }
}
