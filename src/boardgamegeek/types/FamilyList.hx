package boardgamegeek.types;

import boardgamegeek.types.Family.FamilyData;


typedef FamilyList =
{
    @:list('item') var things: Array<FamilyData>;
}
