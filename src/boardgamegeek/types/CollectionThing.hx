package boardgamegeek.types;

typedef CollectionThingStatus =
{
    @:attr var own: Int;
    @:attr var prevowned: Int;
    @:attr var fortrade: Int;
    @:attr var want: Int;
    @:attr var wanttoplay: Int;
    @:attr var wanttobuy: Int;
    @:attr var wishlist: Int;
    @:attr var preordered: Int;
    @:attr var lastmodified: String;
}

typedef CollectionThingData =
{
    var name: String;
    @:tag('yearpublished') var year: Int;
    var image: String;
    var thumbnail: String;
    @:tag('numplays') var numPlays: Int;
    var status: CollectionThingStatus;
}

@:forward(name, year, image, thumbnail, numPlays)
abstract CollectionThing(CollectionThingData) from CollectionThingData
{
    public var owned(get, never): Bool;

    inline function get_owned(): Bool
    {
        return this.status.own > 0;
    }

    public var prevOwned(get, never): Bool;

    inline function get_prevOwned(): Bool
    {
        return this.status.prevowned > 0;
    }

    public var forTrade(get, never): Bool;

    inline function get_forTrade(): Bool
    {
        return this.status.fortrade > 0;
    }

    public var want(get, never): Bool;

    inline function get_want(): Bool
    {
        return this.status.want > 0;
    }

    public var wantToPlay(get, never): Bool;

    inline function get_wantToPlay(): Bool
    {
        return this.status.wanttoplay > 0;
    }

    public var wantToBuy(get, never): Bool;

    inline function get_wantToBuy(): Bool
    {
        return this.status.wanttobuy > 0;
    }

    public var wishlist(get, never): Bool;

    inline function get_wishlist(): Bool
    {
        return this.status.wishlist > 0;
    }

    public var lastModified(get, never): String;

    inline function get_lastModified(): String
    {
        return this.status.lastmodified;
    }
}
