package boardgamegeek.types;

import boardgamegeek.types.SearchResult;


typedef SearchResultList =
{
    @:list('item') var items: Array<SearchResultData>;
}
