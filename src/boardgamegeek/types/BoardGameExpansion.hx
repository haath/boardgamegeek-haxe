package boardgamegeek.types;

typedef BoardGameExpansion =
{
    var id: UInt;
    var name: String;
}
