package boardgamegeek.types;

import boardgamegeek.types.HotItem.HotItemData;


typedef HotItemList =
{
    @:list('item') var items: Array<HotItemData>;
}
