package boardgamegeek.types;

enum abstract ThingType(String) from String to String
{
    var BoardGame = 'boardgame';
    var BoardGameExpansion = 'boardgameexpansion';
    var BoardGameAccessory = 'boardgameaccessory';
    var BoardGameFamily = 'boardgamefamily';
    var VideoGame = 'videogame';
    var RpgItem = 'rpgitem';
    var RpgIssue = 'rpgissue';
}
