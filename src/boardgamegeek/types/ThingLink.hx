package boardgamegeek.types;

typedef ThingLink =
{
    @:attr var type: String;
    @:attr var id: UInt;
    @:attr var value: String;
}
