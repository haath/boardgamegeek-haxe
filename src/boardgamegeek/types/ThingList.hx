package boardgamegeek.types;

import boardgamegeek.types.Thing.ThingData;


typedef ThingList =
{
    @:list('item') var things: Array<ThingData>;
}
