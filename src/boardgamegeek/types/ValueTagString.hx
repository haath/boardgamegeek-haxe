package boardgamegeek.types;

typedef ValueTagString =
{
    @:attr @:optional var type: String;
    @:attr var value: String;
}
