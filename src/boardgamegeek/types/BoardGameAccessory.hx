package boardgamegeek.types;

typedef BoardGameAccessory =
{
    var id: UInt;
    var name: String;
}
