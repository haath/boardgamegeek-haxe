package boardgamegeek.types;

typedef SearchResultData =
{
    @:attr var type: String;
    @:attr var id: Int;
    var name: ValueTagString;
    @:optional var yearpublished: ValueTagInt;
}

@:forward(id)
abstract SearchResult(SearchResultData) from SearchResultData
{
    public var name(get, never): String;

    inline function get_name(): String
    {
        return this.name.value;
    }

    public var type(get, never): ThingType;

    inline function get_type(): ThingType
    {
        return this.type;
    }

    public var year(get, never): Null<UInt>;

    inline function get_year(): Null<UInt>
    {
        return this.yearpublished?.value;
    }
}
