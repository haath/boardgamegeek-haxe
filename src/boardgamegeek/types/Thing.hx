package boardgamegeek.types;

typedef ThingData =
{
    @:attr var id: Int;
    @:attr var type: String;
    @:optional var thumbnail: String;
    @:optional var image: String;
    @:optional var description: String;
    @:list('name') var names: Array<ValueTagString>;

    @:optional var yearpublished: ValueTagInt;
    @:optional var minplayers: ValueTagInt;
    @:optional var maxplayers: ValueTagInt;
    @:optional var playingTime: ValueTagInt;
    @:optional var minplaytime: ValueTagInt;
    @:optional var maxplaytime: ValueTagInt;
    @:list('link') var links: Array<ThingLink>;
}

@:forward(id, thumbnail, image, description)
abstract Thing(ThingData) from ThingData
{
    public var name(get, never): String;

    inline function get_name(): String
    {
        var name: String = this.names.filter(n -> n.type == 'primary')[ 0 ].value;

        if (name == null)
        {
            // primary name non-existent?
            // return whatever
            name = this.names[ 0 ].value;
        }

        return name;
    }

    public var type(get, never): ThingType;

    inline function get_type(): ThingType
    {
        return this.type;
    }

    public var year(get, never): Null<UInt>;

    inline function get_year(): Null<UInt>
    {
        return this.yearpublished?.value;
    }

    public var minPlayers(get, never): Null<UInt>;

    inline function get_minPlayers(): Null<UInt>
    {
        return this.minplayers?.value;
    }

    public var maxPlayers(get, never): Null<UInt>;

    inline function get_maxPlayers(): Null<UInt>
    {
        return this.maxplayers?.value;
    }

    public var playTime(get, never): Null<UInt>;

    inline function get_playTime(): Null<UInt>
    {
        return this.playingTime?.value;
    }

    public var minPlayTime(get, never): Null<UInt>;

    inline function get_minPlayTime(): Null<UInt>
    {
        return this.minplaytime?.value;
    }

    public var maxPlayTime(get, never): Null<UInt>;

    inline function get_maxPlayTime(): Null<UInt>
    {
        return this.maxplaytime?.value;
    }

    public var categories(get, never): Array<String>;

    inline function get_categories(): Array<String>
    {
        return this.links.filter(l -> l.type == ThingLinkType.BoardGameCategory).map(l -> l.value);
    }

    public var mechanics(get, never): Array<String>;

    inline function get_mechanics(): Array<String>
    {
        return this.links.filter(l -> l.type == ThingLinkType.BoardGameMechanic).map(l -> l.value);
    }

    public var families(get, never): Array<String>;

    inline function get_families(): Array<String>
    {
        return this.links.filter(l -> l.type == ThingLinkType.BoardGameFamily).map(l -> l.value);
    }

    public var expansions(get, never): Array<BoardGameExpansion>;

    inline function get_expansions(): Array<BoardGameExpansion>
    {
        return this.links.filter(l -> l.type == ThingLinkType.BoardGameExpansion).map(l ->
            {id: l.id, name: l.value});
    }

    public var accessories(get, never): Array<BoardGameAccessory>;

    inline function get_accessories(): Array<BoardGameAccessory>
    {
        return this.links.filter(l -> l.type == ThingLinkType.BoardGameAccessory).map(l ->
            {id: l.id, name: l.value});
    }

    public var implementations(get, never): Array<String>;

    inline function get_implementations(): Array<String>
    {
        return this.links.filter(l -> l.type == ThingLinkType.BoardGameImplementation).map(l -> l.value);
    }

    public var designers(get, never): Array<String>;

    inline function get_designers(): Array<String>
    {
        return this.links.filter(l -> l.type == ThingLinkType.BoardGameDesigner).map(l -> l.value);
    }

    public var artists(get, never): Array<String>;

    inline function get_artists(): Array<String>
    {
        return this.links.filter(l -> l.type == ThingLinkType.BoardGameArtist).map(l -> l.value);
    }

    public var publishers(get, never): Array<String>;

    inline function get_publishers(): Array<String>
    {
        return this.links.filter(l -> l.type == ThingLinkType.BoardGamePublisher).map(l -> l.value);
    }
}
