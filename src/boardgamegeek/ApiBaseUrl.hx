package boardgamegeek;

enum abstract ApiBaseUrl(String)
{
    var BoardGameGeek = 'https://boardgamegeek.com';
    var RpgGeek = 'https://rpggeek.com';
    var VideoGameGeek = 'https://videogamegeek.com';
}
